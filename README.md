# CppMandelbrot

A realtime mandelbrot renderer I made as an atempt to learn more about c++ and SDL.

## Controls
 - Arrows for movement.
 - \+ and \- for increasing max iteration variable (to increase rendering precision) 
 - \* and / to increase/decrease zoom

## Posible optimalizations
 - add thread pooling
 - find faster sorting algorithm for the calculatedPixels vector
 - render only pixels in the mandelbrot, color it using shaders (not sure, if it's going to look similiar enough to cpu color rendering)
 - render to SDL_Surface instead of render_points
 - just use cuda instead
 - calculate mandelbrot only for new points (Only applicable when moving camera around), center of the image stays the same, only offseted to the side