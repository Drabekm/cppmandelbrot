#include <SDL.h>
#include <unordered_map>
#include <vector>
#include <mutex>

using namespace std;

struct ComplexNumber {
	double r;
	double c;
};

struct CalculatedPixel {
	SDL_Point point;
	int lastIterationInSet;
};

struct less_than_key
{
	inline bool operator() (const CalculatedPixel& struct1, const CalculatedPixel& struct2)
	{
		return (struct1.lastIterationInSet < struct2.lastIterationInSet);
	}
};

class MandelbrotCalculator
{
public:
	int iterationLimit;

	MandelbrotCalculator(int screenWidth, int screenHeight, int initialIterations, SDL_Renderer* renderer);

	~MandelbrotCalculator();

	void MoveHorizontal(double delta);
	void MoveVertical(double delta);
	void IncrementIterations();
	void DecrementIterations();
	void ZoomIn();
	void ZoomOut();
	void RenderFrame();

private:
	double leftBorder = -2;
	double rightBorder = 1;
	double topBorder = 1;
	double bottomBorder = -1;
	double scale = 1;

	int screenWidth;
	int screenHeight;

	int threadCount;

	bool renderIsOnlyIterationIncrement;

	SDL_Renderer* renderer;
	vector<CalculatedPixel> calculatedPixels;
	vector<SDL_Point> pointsToRender;
	vector<thread> threads;
	atomic_int threadSemaphores[40];
	vector<int> threadStarts;
	vector<int> threadEnds;
	mutex calculatedPixelsMutex;

	void CalculatePixelsForArea(int startX, int endX, int startY, int endY, atomic_int* semaphore, int id);

	double MapToReal(int x, double minR, double deltaX);
	double MapToImaginary(int y, double minI, double deltaY);

	int WaitForThreads();
	void StartThreads();

	ComplexNumber GetComplexNumber(double real, double imaginary);
};