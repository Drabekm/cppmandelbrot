#define FMT_HEADER_ONLY
#include <SDL.h>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <fmt/core.h>

#include "MandelbrotCalculator.h"
#include "Timer.h"
#include <SDL_ttf.h>
#include "LogHelper.h"
using namespace std;

//Screen dimension constants
const int SCREEN_WIDTH = 640; //1024
const int SCREEN_HEIGHT = 480; //576



//Starts up SDL and creates window
bool init();

//Frees media and shuts down SDL
void close();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

//Globally used font
TTF_Font* gFont = NULL;

SDL_Texture* fpsTexture;

void PrintFrameRate(int* lastTics, int* countedFrames, Timer fpsTimer);

bool init()
{
	bool initSuccessful = true;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		LogHelper::LogText(std::format("SDL could not initialize! SDL_Error: %s", SDL_GetError()));
		return false;
	}

	gWindow = SDL_CreateWindow("Mandelbrot boi", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (gWindow == NULL)
	{
		LogHelper::LogText(std::format("Window could not be created! SDL_Error: %s", SDL_GetError()));
		return false;
	}

	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
	if (gRenderer == NULL)
	{
		LogHelper::LogText(std::format("Accelerated renderer could not be created! SDL Error: %s", SDL_GetError()));
		LogHelper::LogText("Trying software rendering...");


		gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_SOFTWARE);
		if (gRenderer == NULL)
		{
			LogHelper::LogText(std::format("Software renderer could not be created! SDL Error: %s", SDL_GetError()));
			return false;
		}
	}

	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

	if (TTF_Init() < 0)
	{
		LogHelper::LogText(std::format("SDL_TTF could not be initialized! SDL Error: %s", TTF_GetError()));
		return false;
	}

	gFont = TTF_OpenFont("F:/Programovani/cppmandelbrot/CppMandelbrot/x64/Debug/lazy.ttf", 28);
	if (gFont == NULL)
	{
		LogHelper::LogText(std::format("Font could not be loaded! SDL Error: %s", TTF_GetError()));
		return false;
	}

	return true;
}

void close()
{
	//Deallocate renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;

	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}

int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		close();
		return 0;
	}

	MandelbrotCalculator* mandelbrotCalculator = new MandelbrotCalculator(SCREEN_WIDTH, SCREEN_HEIGHT, 20, gRenderer);
	SDL_Event e;

	//fps counter
	Timer fpsTimer;
	fpsTimer.start();
	int countedFrames = 0;
	int lastTics = 5; // any non zero value to prevent division by zero on the first frame

	bool quit = false;

	while (!quit)
	{
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_UP:
					mandelbrotCalculator->MoveVertical(-0.1);
					break;
				case SDLK_LEFT:
					mandelbrotCalculator->MoveHorizontal(-0.1);
					break;
				case SDLK_RIGHT:
					mandelbrotCalculator->MoveHorizontal(0.1);
					break;
				case SDLK_DOWN:
					mandelbrotCalculator->MoveVertical(0.1);
					break;
				case SDLK_KP_PLUS:
					mandelbrotCalculator->IncrementIterations();
					break;
				case SDLK_KP_MINUS:
					mandelbrotCalculator->DecrementIterations();
					break;
				case SDLK_KP_DIVIDE:
					mandelbrotCalculator->ZoomOut();
					break;
				case SDLK_KP_MULTIPLY:
					mandelbrotCalculator->ZoomIn();
					break;
				}

			}

		}

		SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF); // black
		SDL_RenderClear(gRenderer);


		mandelbrotCalculator->RenderFrame();


		PrintFrameRate(&lastTics, &countedFrames, fpsTimer);

		SDL_RenderPresent(gRenderer);

		countedFrames++;

	}

	close();

	return 0;
}

void PrintFrameRate(int* lastTics, int* countedFrames, Timer fpsTimer)
{
	int currentTics = fpsTimer.getTicks();

	int deltaTics = currentTics - *lastTics;

	if (deltaTics == 0)
	{
		deltaTics = 1;
	}

	int currentFps = 1000 / deltaTics;
	*lastTics = currentTics;

	int avgFPS = *countedFrames / (fpsTimer.getTicks() / 1000.f);
	if (avgFPS > 2000000)
	{
		avgFPS = 0;
	}

	SDL_Color textColor = { 255, 255, 0 };
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, ("FPS: " + to_string(currentFps)).c_str(), textColor);
	fpsTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
	SDL_FreeSurface(textSurface);

	SDL_Rect renderQuad = { 0, 0, 150, 100 };

	//Render to screen
	SDL_RenderCopyEx(gRenderer, fpsTexture, NULL, &renderQuad, 0, NULL, SDL_FLIP_NONE);
}

ComplexNumber GetComplexNumber(double r, double c)
{
	ComplexNumber complexNumber;
	complexNumber.r = r;
	complexNumber.c = c;
	return complexNumber;
}