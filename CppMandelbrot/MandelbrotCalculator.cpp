#include "MandelbrotCalculator.h"
#include <SDL.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iterator>
#include <mutex>
#include <thread>
#include <numeric>

using namespace std;


MandelbrotCalculator::MandelbrotCalculator(int screenWidth, int screenHeight, int intialIterations, SDL_Renderer* renderer)
{
	this->renderer = renderer;
	this->screenWidth = screenWidth;
	this->screenHeight = screenHeight;
	this->iterationLimit = intialIterations;
	this->threadCount = 16;

	int pixelsToCalculate = screenWidth * screenHeight;
	this->calculatedPixels.reserve(pixelsToCalculate);
	this->pointsToRender.reserve(pixelsToCalculate);


	for (int i = 0; i < pixelsToCalculate; i++)
	{
		this->calculatedPixels.push_back(CalculatedPixel{});
		this->pointsToRender.push_back(SDL_Point{});
	}

	for (int i = 0; i < threadCount; i++)
	{
		int heightStart = (screenHeight / threadCount) * i;
		int heightEnd = (screenHeight / threadCount) * (1 + i);

		this->threadSemaphores[i] = 0;
		this->threadStarts.push_back(heightStart);
		this->threadEnds.push_back(heightEnd);

		std::thread renderingThread([this, i, heightStart, heightEnd] {this->CalculatePixelsForArea(0, this->screenWidth, heightStart, heightEnd, &this->threadSemaphores[i], i ); });
		renderingThread.detach();
	}
}

MandelbrotCalculator::~MandelbrotCalculator()
{
	calculatedPixels.clear();
	calculatedPixels.resize(0);
	pointsToRender.clear();
	pointsToRender.resize(0);
}

int MandelbrotCalculator::WaitForThreads() 
{
	int result = 0;
	for (int i = 0; i < threadCount; i++)
	{
		result += threadSemaphores[i];
	}

	return result;
}

void MandelbrotCalculator::StartThreads() 
{
	for (int i = 0; i < threadCount; i++)
	{
		threadSemaphores[i] = 1;
	}
}

void MandelbrotCalculator::RenderFrame()
{
	// render view and store the result for each pixel into single vector, then sort it by number of iteration needed for a point
	// after that, take all pixels with same iteration number and make a render call with them
	StartThreads();

	while (WaitForThreads() != 0)
	{

	}

	renderIsOnlyIterationIncrement = false;

	std::sort(calculatedPixels.begin(), calculatedPixels.end(), less_than_key());
	int lastCount = 0;
	int count = 0;
	int totalCount = 0;
	int lastIteration = calculatedPixels[0].lastIterationInSet;

	for (auto& pixel : calculatedPixels)
	{
		if (pixel.lastIterationInSet != lastIteration)
		{
			if (pixel.lastIterationInSet != iterationLimit)
			{
				SDL_SetRenderDrawColor(renderer, pixel.lastIterationInSet % 200, 30, 30, 0xFF);
				SDL_RenderDrawPoints(renderer, &pointsToRender[lastCount], count - lastCount);
			}
			
			lastCount = count;
			lastIteration = pixel.lastIterationInSet;
		}
		
		pointsToRender[count] = pixel.point;
		count++;
	}
}

void MandelbrotCalculator::CalculatePixelsForArea(int startX, int endX, int startY, int endY, atomic_int* semaphore, int id)
{
	while (true)
	{
		while ((* semaphore) != 1)
		{
			// taking up all the threads and then just make them do nothing is pretty dirty.
			// but it gives me more SPEEED since I don't have to start new threads all the time
		}

		ComplexNumber currentPoint = GetComplexNumber(0.0, 0.0);
		ComplexNumber result = GetComplexNumber(0.0, 0.0);
		ComplexNumber resultSquared = GetComplexNumber(0.0, 0.0);

		double deltaReal = (rightBorder * scale - leftBorder * scale) / screenWidth;
		double deltaImaginary = (topBorder * scale - bottomBorder * scale) / screenHeight;
		ComplexNumber delta = GetComplexNumber(deltaReal, deltaImaginary);

		for (int y = startY; y < endY; ++y)
		{
			currentPoint.c = MapToImaginary(y, bottomBorder, delta.c);
			for (int x = startX; x < endX; ++x)
			{
				CalculatedPixel* pixel = &calculatedPixels[screenWidth * y + x];

				if (renderIsOnlyIterationIncrement && pixel->lastIterationInSet <= iterationLimit - 1)
				{
					continue;
				}

				result.r = 0;
				result.c = 0;
				resultSquared.r = 0;
				resultSquared.c = 0;
				currentPoint.r = MapToReal(x, leftBorder, delta.r);

				int iteration;
				for (iteration = 1; iteration < iterationLimit && resultSquared.r + resultSquared.c < 4.0; ++iteration)
				{
					resultSquared.r = result.r * result.r;
					resultSquared.c = result.c * result.c;

					result.c = 2.0 * result.r * result.c + currentPoint.c;
					result.r = resultSquared.r - resultSquared.c + currentPoint.r;
				}

				pixel->point.x = x;
				pixel->point.y = y;
				pixel->lastIterationInSet = iteration;
			}
		}
		(*semaphore) = 0;
	}
}

void MandelbrotCalculator::MoveHorizontal(double delta)
{
	leftBorder = leftBorder + delta * scale; // multiply by scale to make camera speed apropriate to current zoom level
	rightBorder = rightBorder + delta * scale;
}

void MandelbrotCalculator::MoveVertical(double delta)
{
	topBorder = topBorder + delta * scale;
	bottomBorder = bottomBorder + delta * scale;
}

void MandelbrotCalculator::IncrementIterations()
{
	iterationLimit++;
	renderIsOnlyIterationIncrement = true;
}

void MandelbrotCalculator::DecrementIterations()
{
	iterationLimit--;
	renderIsOnlyIterationIncrement = true;
}

void MandelbrotCalculator::ZoomIn()
{
	scale -= 0.1 * scale; // multiply by scale to make camera speed apropriate to current zoom level
}

void MandelbrotCalculator::ZoomOut()
{
	scale += 0.1 * scale;
}

double MandelbrotCalculator::MapToReal(int x, double minR, double deltaX)
{
	return x * deltaX + minR;
}

double MandelbrotCalculator::MapToImaginary(int y, double minI, double deltaY)
{
	return y * deltaY + minI;
}

ComplexNumber MandelbrotCalculator::GetComplexNumber(double real, double imaginary)
{
	ComplexNumber complexNumber;
	complexNumber.r = real;
	complexNumber.c = imaginary;
	return complexNumber;
}
